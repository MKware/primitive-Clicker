﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace primitiveClicker
{
    public sealed partial class MainPage : Page
    {
        DispatcherTimer dispatcherTimerU1;
        DateTimeOffset startTimeU1;
        DateTimeOffset lastTimeU1;
        int timesTickedU1 = 1;
        int u1AmountInt = 0;
        DispatcherTimer dispatcherTimerU2;
        DateTimeOffset startTimeU2;
        DateTimeOffset lastTimeU2;
        int timesTickedU2 = 1;
        int u2AmountInt = 0;
        DispatcherTimer dispatcherTimerU3;
        DateTimeOffset startTimeU3;
        DateTimeOffset lastTimeU3;
        int timesTickedU3 = 1;
        int u3AmountInt = 0;
        public void dispatcherTimerU1Setup()
        {
            dispatcherTimerU1 = new DispatcherTimer();
            dispatcherTimerU1.Tick += dispatcherTimerU1_Tick;
            dispatcherTimerU1.Interval = new TimeSpan(0, 0, 1);
            startTimeU1 = DateTimeOffset.Now;
            lastTimeU1 = startTimeU1;
            dispatcherTimerU1.Start();
        }
        public void dispatcherTimerU2Setup()
        {
            dispatcherTimerU2 = new DispatcherTimer();
            dispatcherTimerU2.Tick += dispatcherTimerU2_Tick;
            dispatcherTimerU2.Interval = new TimeSpan(0, 0, 1);
            startTimeU2 = DateTimeOffset.Now;
            lastTimeU2 = startTimeU2;
            dispatcherTimerU2.Start();
        }
        public void dispatcherTimerU3Setup()
        {
            dispatcherTimerU3 = new DispatcherTimer();
            dispatcherTimerU3.Tick += dispatcherTimerU3_Tick;
            dispatcherTimerU3.Interval = new TimeSpan(0, 0, 1);
            startTimeU3 = DateTimeOffset.Now;
            lastTimeU3 = startTimeU3;
            dispatcherTimerU3.Start();
        }
        void dispatcherTimerU1_Tick(object sender, object e)
        {
            DateTimeOffset time = DateTimeOffset.Now;
            TimeSpan span = time - lastTimeU1;
            lastTimeU1 = time;
            timesTickedU1++;
            int ptValueLn = points.Text.Length;
            switch (ptValueLn)
            {
                case 1:
                    pointsShadow.Text = "000000000 ";
                    break;
                case 2:
                    pointsShadow.Text = "00000000  ";
                    break;
                case 3:
                    pointsShadow.Text = "0000000   ";
                    break;
                case 4:
                    pointsShadow.Text = "000000    ";
                    break;
                case 5:
                    pointsShadow.Text = "00000     ";
                    break;
                case 6:
                    pointsShadow.Text = "0000      ";
                    break;
                case 7:
                    pointsShadow.Text = "000       ";
                    break;
                case 8:
                    pointsShadow.Text = "00        ";
                    break;
                case 9:
                    pointsShadow.Text = "0         ";
                    break;
                case 10:
                    pointsShadow.Text = "          ";
                    break;
            }
            if (timesTickedU1 == 4)
            {
                string bPtValueString = points.Text;
                int bPtValueInt = Convert.ToInt32(bPtValueString);
                points.Text = Convert.ToString(bPtValueInt + 3);
                timesTickedU1 = 1;
            }
        }
        void dispatcherTimerU2_Tick(object sender, object e)
        {
            DateTimeOffset time = DateTimeOffset.Now;
            TimeSpan span = time - lastTimeU2;
            lastTimeU2 = time;
            timesTickedU2++;
            int ptValueLn = points.Text.Length;
            switch (ptValueLn)
            {
                case 1:
                    pointsShadow.Text = "000000000 ";
                    break;
                case 2:
                    pointsShadow.Text = "00000000  ";
                    break;
                case 3:
                    pointsShadow.Text = "0000000   ";
                    break;
                case 4:
                    pointsShadow.Text = "000000    ";
                    break;
                case 5:
                    pointsShadow.Text = "00000     ";
                    break;
                case 6:
                    pointsShadow.Text = "0000      ";
                    break;
                case 7:
                    pointsShadow.Text = "000       ";
                    break;
                case 8:
                    pointsShadow.Text = "00        ";
                    break;
                case 9:
                    pointsShadow.Text = "0         ";
                    break;
                case 10:
                    pointsShadow.Text = "          ";
                    break;
            }
            if (timesTickedU2 == 3)
            {
                string bPtValueString = points.Text;
                int bPtValueInt = Convert.ToInt32(bPtValueString);
                points.Text = Convert.ToString(bPtValueInt + 4);
                timesTickedU2 = 1;
            }
        }
        void dispatcherTimerU3_Tick(object sender, object e)
        {
            DateTimeOffset time = DateTimeOffset.Now;
            TimeSpan span = time - lastTimeU3;
            lastTimeU3 = time;
            timesTickedU3++;
            int ptValueLn = points.Text.Length;
            switch (ptValueLn)
            {
                case 1:
                    pointsShadow.Text = "000000000 ";
                    break;
                case 2:
                    pointsShadow.Text = "00000000  ";
                    break;
                case 3:
                    pointsShadow.Text = "0000000   ";
                    break;
                case 4:
                    pointsShadow.Text = "000000    ";
                    break;
                case 5:
                    pointsShadow.Text = "00000     ";
                    break;
                case 6:
                    pointsShadow.Text = "0000      ";
                    break;
                case 7:
                    pointsShadow.Text = "000       ";
                    break;
                case 8:
                    pointsShadow.Text = "00        ";
                    break;
                case 9:
                    pointsShadow.Text = "0         ";
                    break;
                case 10:
                    pointsShadow.Text = "          ";
                    break;
            }
            if (timesTickedU3 == 2)
            {
                string bPtValueString = points.Text;
                int bPtValueInt = Convert.ToInt32(bPtValueString);
                points.Text = Convert.ToString(bPtValueInt + 6);
                timesTickedU3 = 1;
            }
        }
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string bPtValueString = points.Text;
            int bPtValueInt = Convert.ToInt32(bPtValueString);
            points.Text = Convert.ToString(bPtValueInt + 1);
            int ptValueLn = points.Text.Length;
            switch (ptValueLn)
            {
                case 1:
                    pointsShadow.Text = "000000000 ";
                    break;
                case 2:
                    pointsShadow.Text = "00000000  ";
                    break;
                case 3:
                    pointsShadow.Text = "0000000   ";
                    break;
                case 4:
                    pointsShadow.Text = "000000    ";
                    break;
                case 5:
                    pointsShadow.Text = "00000     ";
                    break;
                case 6:
                    pointsShadow.Text = "0000      ";
                    break;
                case 7:
                    pointsShadow.Text = "000       ";
                    break;
                case 8:
                    pointsShadow.Text = "00        ";
                    break;
                case 9:
                    pointsShadow.Text = "0         ";
                    break;
                case 10:
                    pointsShadow.Text = "          ";
                    break;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string bPtValueString = points.Text;
            int bPtValueInt = Convert.ToInt32(bPtValueString);

            if (bPtValueInt >= 50)
            {
                points.Text = Convert.ToString(bPtValueInt - 50);
                u1AmountInt += 1;
                u1Amount.Text = Convert.ToString(u1AmountInt) + "x";
                dispatcherTimerU1Setup();
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            string bPtValueString = points.Text;
            int bPtValueInt = Convert.ToInt32(bPtValueString);

            if (bPtValueInt >= 125)
            {
                points.Text = Convert.ToString(bPtValueInt - 125);
                u2AmountInt += 1;
                u2Amount.Text = Convert.ToString(u2AmountInt) + "x";
                dispatcherTimerU2Setup();
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            string bPtValueString = points.Text;
            int bPtValueInt = Convert.ToInt32(bPtValueString);

            if (bPtValueInt >= 210)
            {
                points.Text = Convert.ToString(bPtValueInt - 210);
                u3AmountInt += 1;
                u3Amount.Text = Convert.ToString(u3AmountInt) + "x";
                dispatcherTimerU3Setup();
            }
        }
    }
}
