# Primitive Clicker
[![Project Status: Concept – Minimal or no implementation has been done yet, or the repository is only intended to be a limited example, demo, or proof-of-concept.](https://www.repostatus.org/badges/latest/concept.svg)](https://www.repostatus.org/#concept)

***
## Description
A short and simple clicker game made with UWP.
Works on Windows 10 Mobile too.
***
## Usage
### Installation
1. Get `package.zip` from "Releases"
2. Extract `pC` folder

3.

##### For ARM devices:
Install all packages from `Dependencies\ARM` folder.
##### For x86_64 devices:
Install all packages from `Dependencies\x64` AND `Dependencies\x86` folders.

4. Install `primitiveClicker_1.0.0.0_x86_x64_arm_Release.cer`
5. At last, install `primitiveClicker_1.0.0.0_x86_x64_arm_Release.appxbundle`

###### Notes
If you are going to install it on Windows 10 Mobile, don't use App Deployment software from Windows Phone 8.1 SDK. Use App Installer from Microsoft.

### Source code
1. Clone repository
2. Open `PrimitiveClicker.sln` in VS 2019 or higher

### Compiling
1. Go to VS menu on `Project -> Publish -> Create App Package...`
2. Follow wizard's instructions
3. Compiled binaries can be found in `AppPackages` folder